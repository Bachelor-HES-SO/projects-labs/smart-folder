# Searchfolder

> Raed Abdennadher – Orphée Antoniadis

## Introduction

L'objectif du projet était d'implémenter en langage C et sur la base d'appels système Unix, une idée similaire aux Smart Folders sous MacOS ou Search Folders sous GNU/Linux. Le but pour l'utilisateur est de pouvoir créer un répertoire « virtuel » d'un type spécial contenant le résultat d'une requête de recherche en tant que liens symboliques vers les fichiers originaux du système de fichier.

## Schéma bloc du programme

![](./doc/RapportFinal/process_diagram.png)                    

## Méthodologie

Notre programme est divisé en 3 grandes parties :

* Une partie gérant toutes les fonctions « utilitaires ». C’est la partie cachée du programme mais une des plus importantes. Elle va permettre d’implémenter toute la logique du searchfolder en fournissant des fonctions de gestion de structures telles que les table de hachage, les listes, les appels systèmes, etc…

* La deuxième partie concerne l’évaluation des critères passés en argument et la création d’une structure qui va contenir les éléments indispensables au filtrage des fichiers à insérer dans le searchfolder.

* La troisième et dernière partie est l’implémentation de toute la logique de parcourt des fichiers, de remplissage du searchfolder et d’arrêt du programme. 

## Partie 1 : Librairies « utilitaires »

### Le « wrapper » de commandes

Cette librairie contient toutes les inclusions de librairies c, les defines utilisés dans tout le programme mais aussi les fonctions gérant les appels systèmes. Il y a entre autres les fonctions pour créer, supprimer, ouvrir et fermer un répertoire, une fonction pour créer un lien symbolique et qui va gérer les doublons (deux fichiers qui ont le même nom mais qui ne pointent pas vers le même fichier). Il y a aussi les fonctions pour supprimer, lire et écrire dans un fichier. 

### La gestion des tables de hachage

Nous avons décidé, pour stocker tous les chemins d’accès aux fichiers qui seront parcourus, d’implémenter une librairie de gestion des tables de hachage. Pour créer cette librairie, nous avons d’abord eu à créer notre propre fonction de hachage d’une chaîne de caractères. Une première clé de hachage va être créée en faisant la somme des valeurs de chaque caractère du chemin d’accès dans la table ASCII. Si une collision a lieu, nous créons une seconde clé de hachage qui va être ajoutée à la clé tant qu’une place libre n’est pas trouvée. Afin d’éviter des boucles infinies dans la table de hachage, nous prenons comme taille de tableau le nombre premier supérieur à la taille indiquée par l’utilisateur. Cette taille est donnée lors de l’appel à la fonction de création d’une table.

La fonction d’insertion d’un chemin d’accès dans une table fait donc appel à ces fonctions de création dé clé de hachage. Si elle tombe sur une case déjà prise elle va donc appeler la fonction de double hachage et si elle tombe sur un élément qui existe déjà, elle va s’arrêter et retourner un code d’erreur. Dans le cas où la table dépasse les 75% de remplissage, nous agrandissons l’espace allouer (en le multipliant par 1.5).

### La gestion des listes

Nous avons créé une implémentation générique des listes en pensant en avoir plus besoin dans le programme. Finalement, nous utilisons les listes que pour créer une pile lors de la gestion des conditions. Cette librairie laisse à disposition les fonctions d’inclusions, de suppression et de recherche d’un élément dans une liste.

### Le logger

Dans ce module, nous avons implémenté la gestion des logs du programme. Tout message d’erreur ou d’avertissement passe par la fonction logger qui va l’écrire dans la sortie choisie (sortie standard, fichier…). Selon le niveau de log choisi, cette fonction affiche ou pas les messages. On avait choisi le niveau LOG_LEVEL pour pouvoir tracer tous les notifications.

## Partie 2 : Le parser

### Analyse des arguments

La première chose que nous faisons lors du démarrage du programme est de regarder le nombre d’arguments passés par l’utilisateur. S’il n’y a pas assez d’arguments, un petit mot est imprimé sur la console sur l’utilisation du programme. Une fois le bon nombre d’arguments passés, ils vont tout de suite être passés à notre fonction parser. Cette fonction va analyser les chaînes de caractères afin de savoir quel critère est demandé.

### La structure de données

Nous avons décidé de créer une structure de données qui va contenir les arguments du critère de l’utilisateur. Cette structure va donc représenter un critère. Une autre structure va contenir un tableau de cette structure critère (1 pour chaque critère passé par l’utilisateur) et un tableau de chaînes de caractères qui va contenir dans l’ordre chaque étape du calcul à faire pour filtrer la liste de fichiers. Dans ce tableau, un chiffre représentera un critère (sa position dans le tableau de critères). S’il y a un opérateur logique, il sera ajouté tel quel dans le tableau.

Exemple d’expression : 0 1 and 2 or not

### Le filtre

Bien que le filtre fait partie du moteur de recherche, c’est lui qui va exploiter la structure du parser. Son fonctionnement est donc lié à celui du parser. Le filtre va prendre en argument cette structure ainsi que la liste complète des fichiers. Il va ensuite créer une table de hachage par critère puis parcourir les noms des fichiers et les analyser. Si le fichier analysé correspond au critère actuel, il est ajouté à la table de hachage associée. Il va ainsi créer des tables de hachages filtrées. Une fois chaque critère parcouru et chaque fichier analysé, il va regarder combien de tables ont été créées. Si une seul a été créée il va juste regarder s’il n’y a pas un opérateur logique not, si oui il va l’inverser et la renvoyée, sinon il va la renvoyer tel quel. 

Dans le cas où il y a plusieurs tables filtrées donc plusieurs critères, il va parcourir le tableau de chaînes de caractères décrit précédemment et va faire les opérations en utilisant la méthode de la notation polonaise inverse. La pile décrite précédemment est donc utilisée ici. Reprenons l’exemple précédent. Le filtre va insérer la première table filtrée dans la pile, puis la deuxième. Elle va ensuite retirer les deux tables de la pile et faire une intersection de tables (opérateur and) puis ajouté le résultat dans la pile. La suite se déroule de la même manière sauf que pour l’opérateur or on fait une union de tables.

## Partie 3 : Le moteur de recherche

### Le crawler

Notre crawler est une fonction récursive qui va parcourir tous les éléments en dessous du searchpath. Pour parcourir les fichiers, nous utilisons la librairie dirent.h et faisons des tests pour analyser les fichiers. Les fichiers sont ainsi tous insérés dans la table de hachage. Nous avons une condition d’arrêt si le crawler n’arrive pas à insérer dans la table ça veut dire que nous sommes sur un chemin déjà parcouru et qu’il faut s’arrêter. La table de hachage est ensuite renvoyée par la fonction.

Cependant, nous n’envoyons pas cette table au filtre. Nous procédons d’abord à un calcul pour détecter si de nouveaux fichiers ont été ajouté. En effet, le programme doit pouvoir tourner en boucle et modifier le contenu du searchfolder dynamiquement. Pour détecter si de nouveaux fichiers nous créons une table de hachage qui va sauvegarder l’ancien état de la table de hachage qui contient tous les chemins d’accès aux fichiers. En comparant ces deux tables, nous pouvons ainsi savoir quels sont les nouveaux fichiers mais aussi ceux qui ont été supprimés et par conséquent rafraichir les liens symboliques. La table contenant seulement les nouveaux fichiers est pour finir envoyée au filtre.

### Le linker

Le linker va simplement créer les liens symboliques sur les fichiers contenu par la table de hachage filtrée. C’est lui aussi qui va rafraichir les liens symboliques en utilisant la méthode décrite précédemment.

### La gestion du processus

Au début de l’appel au moteur de recherche, nous appelons la fonction fork afin de lancer la recherche en arrière-plan. A ce même moment, nous stockons le PID du processus fils dans un fichier caché nommé .pid.txt sauvegardé dans le searchfolder. Pour stopper le processus et supprimer le searchfolder proprement, nous utilisons un thread qui tourne à côté du processus et qui attend qu’on lui envoi le signal SIGINT. Lorsque l’utilisateur rentre l’argument pour supprimer le searchfolder, le programme envoi donc ce signal au processus lié au searchfolder  à arrêter (le processus est trouvé grâce au PID stocké précédemment). Ainsi, le programme sort de sa boucle, libère la mémoire allouée et se termine proprement.