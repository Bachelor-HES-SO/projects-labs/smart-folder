#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>

int main (int argc, char** argv) {
   DIR* dir = NULL;
   struct dirent* file = NULL;
   char* dir_path = "/bin";
   dir = opendir(dir_path);

   if (dir == NULL) {
      perror("");
      exit(1);
   }

   int cnt = 0;
   while ((file = readdir(dir)) != NULL) {
      if (cnt > 1) {
         char file_path[sizeof(dir_path) + sizeof(file->d_name)];
         strcpy(file_path, dir_path);
         strcat(file_path,"/");
         strcat(file_path,file->d_name);
         printf("%s\n", file_path);
      }
      cnt++;
   }

   if (closedir(dir) == -1) {
      perror("");
      exit(-1);
   }

   return EXIT_SUCCESS;
}
