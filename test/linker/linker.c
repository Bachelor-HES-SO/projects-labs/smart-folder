#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

int main (int argc, char** argv) {
	if (argc == 3) {
		char* oldpath = argv[1];
		char* newpath = argv[2];
		if (symlink(oldpath,newpath) < 0) {
	      perror("symlink");
			logger(LOG_DEBUG, stderr, "failed to create symlink %s for file %s\n", newpath, oldpath);
	      return EXIT_FAILURE;
	   }
		printf("created symlink %s for %s\n", newpath, oldpath);
	   return EXIT_SUCCESS;
	} else {
		logger(LOG_DEBUG, stderr, "usage: linker <oldpath> <newpath>\n\n"
			"• oldpath is the name of the file that will be link.\n"
			"• newpath is the folder where the file will be link.\n");
		return EXIT_FAILURE;
	}
}
