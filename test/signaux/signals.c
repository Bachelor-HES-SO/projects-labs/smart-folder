#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>

void* stop_searchfolder(void* arg) {
	sigset_t mask, maskold;
	sigfillset(&mask);
	pthread_sigmask(SIG_SETMASK, &mask, &maskold);
	int sig;
	do {
		sigwait(&mask, &sig);
	} while (sig != SIGINT);
	*((int*)arg) = 1;
	puts("Program terminated.");
	return NULL;
}

int main () {
	sigset_t mask, maskold;
	sigfillset(&mask);
	pthread_sigmask(SIG_SETMASK, &mask, &maskold);
	pthread_t stop;
	int finished = 0;
	if (pthread_create(&stop, NULL, stop_searchfolder, &finished) != 0) {
		logger(LOG_DEBUG, stderr, "pthread_create failed!\n");
		return EXIT_FAILURE;
	}
	if (pthread_join(stop, NULL) != 0) {
		logger(LOG_DEBUG, stderr, "pthread_join failed!\n");
		return EXIT_FAILURE;
	}
	while (!finished);
	return EXIT_SUCCESS;
}
