#include <stdlib.h>
#include <stdio.h>
#include "list.h"

void print_int(void* data) {
	int int_data = *((int*)data);
	printf("%d\n",int_data);
}

int main(int argc, char** argv) {
	int size = 10;
	int data[size];
	node_t* list = new_list();
	for (int i = 0; i < size; i++) {
		data[i] = i;
		insert_tail(&list, &data[i]);
	}
	print_list(list,print_int);
	printf("\n");
	print_int(remove_node(&list,0).data);
	printf("\n");
	print_list(list,print_int);
	printf("\n");
	print_list(get_element(list,8),print_int);
	free_list(list);
}
