#ifndef _LIST_H_
#define _LIST_H_

typedef struct node_t {
	void* data;
	struct node_t* next;
} node_t;

node_t* new_list();
int is_empty(node_t* list);
void insert_head(node_t** list, void* data);
void insert_tail(node_t** list, void* data);
node_t remove_node(node_t** list, unsigned int pos);
node_t* get_node(node_t* list, unsigned int pos);
unsigned int size(node_t* list);
void free_list(node_t* list);
void print_list(node_t* list, void(*print)(void* arg));

#endif
