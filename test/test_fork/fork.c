#include <stdio.h>
#include <unistd.h>

int main () {
	int c_pid = -1;
	c_pid = fork();
	if (c_pid == 0) {
		puts("process forked.");
		printf("kill -s SIGINT %d to stop the process\n", getpid());
		while (1);
	} else if (c_pid < 0) {
		perror("fork");
	}
	return 0;
}
