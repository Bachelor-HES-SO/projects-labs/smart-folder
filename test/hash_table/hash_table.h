#include "libs/list.h"

#ifndef _HASH_TABLE_H_
#define _HASH_TABLE_H_

#define STR_SIZE 4095

typedef struct {
	char path[STR_SIZE];
	int id;
	int hash;
	int state; // -1 = deleted, 0 = empty, 1 = full
} element_t;

typedef struct {
	element_t* path_table;
	node_t* path_list;
	unsigned int size;
   unsigned int cnt;
} table_t;

table_t* new_table(int size);
int insert_path(table_t* hash_table, char* path);
void remove_path(table_t* hash_table, char* path);
int search_path(table_t* hash_table, char* path);
void print_table(table_t* hash_table);
void free_table(table_t* hash_table);
table_t* intersect_table(table_t* table1, table_t* table2);
table_t* union_table(table_t* table1, table_t* table2);

#endif
