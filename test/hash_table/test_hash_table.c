#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hash_table.h"

int length = 1;

int main(int argc, char** argv) {
   table_t* table1 = new_table(length);
   // printf("%d\n", table1->size);
   insert_path(table1,"/hello/world");
   insert_path(table1,"/hello/dlrow");
   insert_path(table1,"/hello/world");
   insert_path(table1,"/hello/raed");
   insert_path(table1,"/hello/orphee");
   insert_path(table1,"/hello/steven");
   print_table(table1);
   // table_t* table2 = new_table(length);
   // insert_path(table2,"/hello/world");
   // insert_path(table2,"/olleh/world");
   // print_table(table2);
   // print_table(intersect_table(table1,table2));
   // print_table(union_table(table1,table2));
   free_table(table1);
   // free_table(table2);
}
