#ifndef _PRIME_NUMBERS_H_
#define _PRIME_NUMBERS_H_

int is_prime_number(int num);
int lower_prime_number(int num);
int upper_prime_number(int num);

#endif
