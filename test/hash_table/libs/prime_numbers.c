#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "prime_numbers.h"

int is_prime_number(int num) {
   for (int i = 2; i <= sqrt(num); i++) {
      if ((num % i) == 0) {
         return false;
      }
   }
   return true;
}

int lower_prime_number(int num) {
   if (num <= 3) {
      return 3;
   }
   int cnt = num - 1;
   while (!is_prime_number(cnt)) {
      cnt--;
   }
   return cnt;
}

int upper_prime_number(int num) {
   if (num < 3) {
      return 3;
   }
   int cnt = num + 1;
   while (!is_prime_number(cnt)) {
      cnt++;
   }
   return cnt;
}
