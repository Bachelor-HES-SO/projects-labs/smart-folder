#include <stdlib.h>
#include <stdio.h>
#include "list.h"

node_t* new_list() {
	return NULL;
}

int is_empty(node_t* list) {
	return list == NULL;
}

void insert_head(node_t** list, void* data) {
	node_t* new_node = malloc(sizeof(node_t));
	new_node->data = data;
	new_node->next = *list;
	*list = new_node;
}

void insert_tail(node_t** list, void* data) {
	node_t* new_node = malloc(sizeof(node_t));
	new_node->data = data;
	new_node->next = NULL;
	if (is_empty(*list)) {
		*list = new_node;
	} else {
		node_t* temp = *list;
		while (!is_empty(temp->next)) {
			temp = temp->next;
		}
		temp->next = new_node;
	}
}

node_t remove_node(node_t** list, unsigned int pos) {
	node_t* temp = *list;
	node_t buff = *temp;
	int cnt = 0;
	if (is_empty(*list)) {
		fprintf(stderr,"list is empty\n");
		return buff;
	}
	int list_size = size(*list);
	if (list_size <= pos) {
		fprintf(stderr,"invalid position\n");
		return buff;
	}
	if (pos == 0) {
		*list = (*list)->next;
		free(temp);
		return buff;
	}
	while (cnt < pos-1) {
		temp = temp->next;
		cnt++;
	}
	buff = *(temp->next);
	free(temp->next);
	temp->next = temp->next->next;
	return buff;
}

node_t* get_node(node_t* list, unsigned int pos) {
	if (is_empty(list)) {
		fprintf(stderr,"list is empty\n");
		return list;
	}
	int list_size = size(list);
	if (list_size <= pos) {
		fprintf(stderr,"invalid position\n");
		return NULL;
	}
	node_t* temp = list;
	int cnt = 0;
	while (cnt < pos) {
		temp = temp->next;
		cnt++;
	}
	temp->next = NULL;
	return temp;
}

unsigned int size(node_t* list) {
	node_t* temp = list;
	int cnt = 0;
	if (is_empty(list)) {
		fprintf(stderr,"list is empty\n");
		return cnt;
	}
	while (!is_empty(temp)) {
		cnt++;
		temp = temp->next;
	}
	return cnt;
}

void free_list(node_t* list) {
	node_t* temp = list;
	if (is_empty(list)) {
		fprintf(stderr,"list is empty\n");
	}
	while (!is_empty(list)) {
		list = list->next;
		free(temp);
		temp = list;
	}
}

void print_list(node_t* list, void(*print)(void* arg)) {
	node_t* temp = list;
	if (is_empty(list)) {
		fprintf(stderr,"list is empty\n");
	}
	while (!is_empty(temp)) {
		print(temp->data);
		temp = temp->next;
	}
}
