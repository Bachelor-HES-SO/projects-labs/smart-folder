#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "libs/prime_numbers.h"
#include "hash_table.h"

int get_hash(table_t* hash_table, char* path) {
   int hash = 0;
   int cnt = strlen(path) - 1;
   while (path[cnt] != '/' && cnt != 0) {
      hash = (hash + (int)path[cnt]) % hash_table->size;
      cnt--;
   }
   return hash;
}

int get_double_hash(table_t* hash_table, char* path) {
   int hash = get_hash(hash_table,path);
   int double_hash_size = lower_prime_number(hash_table->size);
   int double_hash = 1 + hash % double_hash_size;
   return double_hash;
}

table_t* new_table(int size) {
   table_t* hash_table = malloc(sizeof(table_t));
   hash_table->size = upper_prime_number(size);
   hash_table->path_table = malloc(sizeof(element_t) * hash_table->size);
   hash_table->path_list = new_list();
   hash_table->cnt = 0;
   return hash_table;
}

void resize_table(table_t* hash_table, int size) {
   table_t* table_buff = new_table(size);
   node_t* list_buff = hash_table->path_list;
   while (!is_empty(list_buff)) {
      element_t* element = (element_t*)(list_buff->data);
      insert_path(table_buff,element->path);
      list_buff = list_buff->next;
   }
   free_list(hash_table->path_list);
   free(hash_table->path_table);
   hash_table->size = table_buff->size;
   hash_table->path_table = table_buff->path_table;
   hash_table->path_list = table_buff->path_list;
   free(table_buff);
}

int insert_path(table_t* hash_table, char* path) {
   if (strlen(path) <= STR_SIZE) {
      if (hash_table->cnt >= (hash_table->size / 1.5)) {
         resize_table(hash_table,(hash_table->size * 2));
      }
      int hash = get_hash(hash_table,path);
      int double_hash = get_double_hash(hash_table,path);
      while (hash_table->path_table[hash].state != 0) {
         if (strcmp(path, hash_table->path_table[hash].path) == 0) return 0;
         hash = (hash + double_hash) % hash_table->size;
      }
      hash_table->cnt++;
      element_t new_element;
      strcpy(new_element.path,path);
      new_element.id = hash_table->cnt;
      new_element.hash = hash;
      new_element.state = 1;
      hash_table->path_table[hash] = new_element;
      insert_head(&(hash_table->path_list),&hash_table->path_table[hash]);
      return 1;
   }
   return 0;
}

void remove_path(table_t* hash_table, char* path) {
   int pos = search_path(hash_table,path);
   if (pos != -1) {
      hash_table->path_table[pos].state = -1;
      hash_table->cnt--;
   }
}

int search_path(table_t* hash_table, char* path) {
   int hash = get_hash(hash_table,path);
   int double_hash = get_double_hash(hash_table,path);
   while (strcmp(path, hash_table->path_table[hash].path) != 0) {
      if (hash_table->path_table[hash].state == 0) return -1;
      hash = (hash + double_hash) % hash_table->size;
   }
   if (hash_table->path_table[hash].state == -1) return -1;
   return hash;
}

void free_table(table_t* hash_table) {
   free_list(hash_table->path_list);
   free(hash_table->path_table);
   free(hash_table);
}

void print_element(void* data) {
   element_t element = *((element_t*)data);
   printf("id : %d\n", element.id);
   printf("content : %s\n", element.path);
   printf("hash : %d\n", element.hash);
   printf("state : %d\n\n", element.state);
}

void print_table(table_t* hash_table) {
   print_list(hash_table->path_list,print_element);
   printf("count : %d\n", hash_table->cnt);
}

table_t* intersect_table(table_t* table1, table_t* table2) {
   table_t* final_table = new_table(table1->size);
   node_t* buff = table2->path_list;
   while (!is_empty(buff)) {
      element_t* element = (element_t*)(buff->data);
      if (search_path(table1,element->path) != -1) {
         insert_path(final_table,element->path);
      }
      buff = buff->next;
   }
   return final_table;
}

table_t* union_table(table_t* table1, table_t* table2) {
   table_t* final_table = new_table(table1->size);
   node_t* buff = table1->path_list;
   while (!is_empty(buff)) {
      element_t* element = (element_t*)(buff->data);
      insert_path(final_table,element->path);
      buff = buff->next;
   }
   buff = table2->path_list;
   while (!is_empty(buff)) {
      element_t* element = (element_t*)(buff->data);
      insert_path(final_table,element->path);
      buff = buff->next;
   }
   return final_table;
}
