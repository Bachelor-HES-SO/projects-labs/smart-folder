var list_8h =
[
    [ "node_t", "structnode__t.html", "structnode__t" ],
    [ "node_t", "list_8h.html#a7856550eaf3d2ba89448e2795b8a744e", null ],
    [ "free_list", "list_8h.html#aa13b0ecb36f4cdbbc2f55e6b9cf1dd16", null ],
    [ "get_node", "list_8h.html#a6d0b50d781695455291f0dd63d601918", null ],
    [ "insert_head", "list_8h.html#afd9fffaa9cb079406050bfb4dfb062ff", null ],
    [ "insert_tail", "list_8h.html#a892dd296a519b13603b2261285357b47", null ],
    [ "is_empty", "list_8h.html#a2470dc01cbfccd978242c9aa5fff1217", null ],
    [ "new_list", "list_8h.html#a52fd8ae050abb2e6a6bd7aa17ad20a42", null ],
    [ "print_list", "list_8h.html#af6f6e7ea883c6a2e36a6bfc6dd653f23", null ],
    [ "remove_node", "list_8h.html#ab931a1e780637e15a9ff7a681778fbbc", null ],
    [ "size", "list_8h.html#a8ffe93a3beaf22d1d8cd6ee35beb1012", null ]
];