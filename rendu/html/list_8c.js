var list_8c =
[
    [ "free_list", "list_8c.html#aa13b0ecb36f4cdbbc2f55e6b9cf1dd16", null ],
    [ "get_node", "list_8c.html#a12def8678b9602312e57e290462c8ae0", null ],
    [ "insert_head", "list_8c.html#afd9fffaa9cb079406050bfb4dfb062ff", null ],
    [ "insert_tail", "list_8c.html#a892dd296a519b13603b2261285357b47", null ],
    [ "is_empty", "list_8c.html#a2470dc01cbfccd978242c9aa5fff1217", null ],
    [ "new_list", "list_8c.html#aaf19d730cd21bbfe036aaf5d39f641f6", null ],
    [ "print_list", "list_8c.html#af6f6e7ea883c6a2e36a6bfc6dd653f23", null ],
    [ "remove_node", "list_8c.html#ab931a1e780637e15a9ff7a681778fbbc", null ],
    [ "size", "list_8c.html#a8ffe93a3beaf22d1d8cd6ee35beb1012", null ]
];