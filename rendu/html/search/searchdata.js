var indexSectionsWithContent =
{
  0: "_cdefghiklmnoprstuw",
  1: "cent",
  2: "cfhlpsw",
  3: "cdfgilmnoprstu",
  4: "cdehnps",
  5: "n",
  6: "_cdgklmnos",
  7: "dp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Macros",
  7: "Pages"
};

