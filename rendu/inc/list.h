/*!
 	\file list.h
 	\brief C'est le header du fichier list.c
	\author Abdennadher Raed & Antoniadis Orphée
 	\version 0.1
 	\date 27 janvier 2017
*/

#ifndef _LIST_H_
/*!
  \def _LIST_H_
  Pour éviter des inclusions multiples
*/
#define _LIST_H_

/*!
	\struct
	Représente un noeud d'une liste
*/
typedef struct node_t {
	void* data;				/**< Données */
	struct node_t* next;	/**< Noeud suivant */
} node_t;

node_t* new_list();
int is_empty(node_t* list);
void insert_head(node_t** list, void* data);
void insert_tail(node_t** list, void* data);
node_t remove_node(node_t** list, unsigned int pos);
node_t* get_node(node_t* list, unsigned int pos);
unsigned int size(node_t* list);
void free_list(node_t* list);
void print_list(node_t* list, void(*print)(void* arg));

#endif
