/*!
 	\file wrapper.h
 	\brief C'est le header du fichier wrapper.c
	\author Abdennadher Raed & Antoniadis Orphée
 	\version 0.1
 	\date 27 janvier 2017
*/

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <ctype.h>
#include <time.h>
#include <pwd.h>
#include <grp.h>
#include <pthread.h>
#include <signal.h>
#include <stdarg.h>
#include "logger.h"

#ifndef _WRAPPER_H_
/*!
  \def _WRAPPER_H_
  Pour éviter des inclusions multiples
*/
#define _WRAPPER_H_

#define INTLEN 10

int my_mkdir(char* dir_path);
int my_rmdir(char* dir_path);
DIR* my_opendir(char* dir_path);
int my_closedir(DIR* dir);
int my_rm(char* pathname);
int my_ln(char* oldpath, char* newpath);
void write_file(char* dir_path, char* file_name);
int read_file(char* dir_path, char* file_name);

#endif
