/*!
 	\file search_engine.h
 	\brief C'est le header du fichier search_engine.c
	\author Abdennadher Raed & Antoniadis Orphée
 	\version 0.1
 	\date 27 janvier 2017
*/

#include "parser.h"
#include "hash_table.h"
#include "list.h"
#include "logger.h"

#ifndef _SEARCH_ENGINE_H_
/*!
  \def _SEARCH_ENGINE_H_
  Pour éviter des inclusions multiples
*/
#define _SEARCH_ENGINE_H_

/*!
  \def CHECK_ERR(expr, msg)
  Si \a expr renvoie une erreur, afficher le message \a msg
*/
#define CHECK_ERR(expr, msg) if (expr) {logger(LOG_DEBUG, stderr, "%s\n", msg);}

/*!
  \def LENGTH
  Représente la taille initiale de la table de hash
*/
#define LENGTH 1000

char* get_filename(char* path);
void crawler(table_t* table, char* search_path);
table_t* filter(table_t* table, expression_t* strct);
void linker(table_t* oldtable, table_t* newtable, table_t* resolved_table, char* dir_path);
void search_engine(char* dir_name, char* search_path, expression_t* strct);

#endif
