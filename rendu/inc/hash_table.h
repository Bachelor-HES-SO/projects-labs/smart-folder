/*!
 	\file hash_table.h
 	\brief C'est le header du fichier hash_table.c
	\author Abdennadher Raed & Antoniadis Orphée
 	\version 0.1
 	\date 27 janvier 2017
*/

#ifndef _HASH_TABLE_H_
/*!
  \def _HASH_TABLE_H_
  Pour éviter des inclusions multiples
*/
#define _HASH_TABLE_H_
/*!
  \def STR_SIZE
  Représente la taille de la chaine de caractère qui contient le path d'un fichier
*/
#define STR_SIZE 4095

/*!
	\struct
	Représente un élément d'une table de hash
*/
typedef struct {
	char path[STR_SIZE];	/**< Path du fichier */
	int hash;				/**< Hash du fichier */
	int state;				/**< Etat de l'élément : 0 = vide, 1 = existe */
} element_t;

/*!
	\struct
	Représente la table de hash
*/
typedef struct {
	element_t* path_table;	/**< Tableau hash effectif */
	int size;				/**< Taille de la tablea de hash*/
	int cnt;				/**< Nombre d'éléments non vide dans la table de hachage */
} table_t;

table_t* new_table(int size);
void resize_table(table_t* table, int size);
void table_copy(table_t* dst, table_t* src);
void clear_table(table_t* table);
int insert_path(table_t* table, char* path);
int search_path(table_t* table, char* path);
void print_table(table_t* table);
void free_table(table_t* table);
table_t* intersect_table(table_t* table1, table_t* table2);
table_t* union_table(table_t* table1, table_t* table2);
table_t* substract_table(table_t* table1, table_t* substracted_table);

#endif
