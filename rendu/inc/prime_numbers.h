/*!
 	\file prime_numbers.h
 	\brief C'est le header du fichier prime_numbers.c
	\author Abdennadher Raed & Antoniadis Orphée
 	\version 0.1
 	\date 27 janvier 2017
*/

#ifndef _PRIME_NUMBERS_H_
/*!
  \def _PRIME_NUMBERS_H_
  Pour éviter des inclusions multiples
*/
#define _PRIME_NUMBERS_H_

int is_prime_number(int num);
int lower_prime_number(int num);
int upper_prime_number(int num);

#endif
