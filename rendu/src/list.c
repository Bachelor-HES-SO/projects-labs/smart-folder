/*!
   \file list.c
   \brief Contient les fonctions pour gérer une liste chainée
   \author Abdennadher Raed & Antoniadis Orphée
   \version 0.1
   \date 27 janvier 2017
*/

#include <stdlib.h>
#include <stdio.h>
#include "../inc/list.h"
#include "../inc/logger.h"

/*!
   \fn node_t* new_list()
   \brief Création d'une nouvelle liste vide
   \return NULL
*/
node_t* new_list() {
	return NULL;
}

/*!
   \fn int is_empty(node_t* list)
   \brief Vérifier si une liste est vide
   \param list Liste chainée à vérifier
   \return \a true si la liste est vide, \a false sinon
*/
int is_empty(node_t* list) {
	return list == NULL;
}

/*!
   \fn void insert_head(node_t** list, void* data)
   \brief Insertion d'un élément à la tête de la liste
   \param list Liste chainée
   \param data Elément à insérer
*/
void insert_head(node_t** list, void* data) {
	node_t* new_node = malloc(sizeof(node_t));
	new_node->data = data;
	new_node->next = *list;
	*list = new_node;
}

/*!
   \fn void insert_tail(node_t** list, void* data)
   \brief Insertion d'un élément à la queue de la liste
   \param list Liste chainée
   \param data Elément à insérer
*/
void insert_tail(node_t** list, void* data) {
	node_t* new_node = malloc(sizeof(node_t));
	new_node->data = data;
	new_node->next = NULL;
	if (is_empty(*list)) {
		*list = new_node;
	} else {
		node_t* temp = *list;
		while (!is_empty(temp->next)) {
			temp = temp->next;
		}
		temp->next = new_node;
	}
}

/*!
   \fn node_t remove_node(node_t** list, unsigned int pos)
   \brief Suppression de l'élément ayant la position \a pos de la liste
   \param list Liste chainée
   \param pos Position de l'élément à supprimer
   \return Le noeud supprimé
*/
node_t remove_node(node_t** list, unsigned int pos) {
	node_t* temp = *list;
	node_t buff = *temp;
	unsigned int cnt = 0;
	if (is_empty(*list)) {
		logger(LOG_DEBUG, stderr,"list is empty\n");
		return buff;
	}
	unsigned int list_size = size(*list);
	if (list_size <= pos) {
		logger(LOG_DEBUG, stderr,"invalid position\n");
		return buff;
	}
	if (pos == 0) {
		*list = (*list)->next;
		free(temp);
		return buff;
	}
	while (cnt < pos-1) {
		temp = temp->next;
		cnt++;
	}
	buff = *(temp->next);
	free(temp->next);
	temp->next = temp->next->next;
	return buff;
}

/*!
   \fn node_t* get_node(node_t* list, unsigned int pos)
   \brief Récupération de l'élément ayant la position \a pos à partir de la liste
   \param list Liste chainée
   \param pos Position de l'élément à récupérer
   \return Pointeur sur le noeud trouvé si trouvé, NULL sinon
*/
node_t* get_node(node_t* list, unsigned int pos) {
	if (is_empty(list)) {
		logger(LOG_DEBUG, stderr,"list is empty\n");
		return list;
	}
	unsigned int list_size = size(list);
	if (list_size <= pos) {
		logger(LOG_DEBUG, stderr,"invalid position\n");
		return NULL;
	}
	node_t* temp = list;
	unsigned int cnt = 0;
	while (cnt < pos) {
		temp = temp->next;
		cnt++;
	}
	//temp->next = NULL;
	return temp;
}

/*!
   \fn unsigned int size(node_t* list)
   \brief Calcul de la taille de la liste
   \param list Liste chainée
   \return La taille de la liste
*/
unsigned int size(node_t* list) {
	node_t* temp = list;
	int cnt = 0;
	if (is_empty(list)) {
		logger(LOG_DEBUG, stderr,"list is empty\n");
		return cnt;
	}
	while (!is_empty(temp)) {
		cnt++;
		temp = temp->next;
	}
	return cnt;
}

/*!
   \fn void free_list(node_t* list)
   \brief Libération de la mémoire alouée par la liste
   \param list Liste chainée
*/
void free_list(node_t* list) {
	node_t* temp = list;
	if (is_empty(list)) {
		logger(LOG_DEBUG, stderr,"list is empty\n");
	}
	while (!is_empty(list)) {
		list = list->next;
		free(temp);
		temp = list;
	}
}

/*!
   \fn void print_list(node_t* list, void(*print)(void* arg))
   \brief Affichage de la liste sur la sortie standrad
   \param list Liste chainée à afficher
   \param print Pointeur sur une fonction d'affichage
*/
void print_list(node_t* list, void(*print)(void* arg)) {
	node_t* temp = list;
	if (is_empty(list)) {
		logger(LOG_DEBUG, stderr,"list is empty\n");
	}
	while (!is_empty(temp)) {
		print(temp->data);
		temp = temp->next;
	}
}
