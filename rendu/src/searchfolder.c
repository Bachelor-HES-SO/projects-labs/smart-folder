/*!
 	\file searchfolder.c
 	\brief Contient le programme principale.
	\author Abdennadher Raed & Antoniadis Orphée
 	\version 0.1
 	\date 27 janvier 2017
*/

#include "../inc/search_engine.h"

/*!
	\fn int main (int argc, char** argv)
 	\brief C'est la fonction d'entrée du programme. 

 	Dans cette fonction,
 	il y a vérification des arguments:
 	- Si \a argc > 4 : création du dossier de recherche et lancement 
 	de la procédure de recherche.
 	- Si \a argc == 3 : arrêt du processus de recherche et suppression
 	du dossier.

 	\param argc Nombre d'arguments passés au programme
 	\param argv Vecteur d'arguments
 	\return \a EXIT_SUCCESS si tout est bien passé, EXIT_FAILURE sinon
*/
int main (int argc, char** argv) {
	if (argc > 4) {
		char* dir_path = argv[1];
		char* searth_path = argv[2];
		if (my_mkdir(dir_path) != 0) return EXIT_FAILURE;
		expression_t* strct = parse(&argv[3],argc-3);
		search_engine(dir_path,searth_path,strct);
		return EXIT_SUCCESS;
	} else if (argc == 3) {
		char* dir_path = argv[2];
		if (strcmp("-d",argv[1]) == 0) {
			int pid = 0;
			if ((pid = read_file(dir_path, "/.pid.txt")) < 0) return EXIT_FAILURE;
			kill(pid,SIGINT);
			my_rmdir(dir_path);
			return EXIT_SUCCESS;
		}
	}
	logger(LOG_DEBUG, stderr, "%s\n", "\nusage: searchfolder <dir_name> <search_path> [expression]\n"
			"       searchfolder -d <dir_name>\n\n"
			"• dir_name is the name of the folder that will be created.\n"
			"• search_path is the path under which the files desired are.\n"
			"• [expression] contains the search criteria.\n"
			"• the second command will delete the smart folder.\n");
	return EXIT_SUCCESS;
}
