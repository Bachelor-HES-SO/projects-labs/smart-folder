/*!
   \file prime_numbers.c
   \brief Contient les fonctions pour gérer les nombres premiers
   \author Abdennadher Raed & Antoniadis Orphée
   \version 0.1
   \date 27 janvier 2017
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "../inc/prime_numbers.h"

/*!
   \fn int is_prime_number(int num)
   \brief Vérification si le nombre \a num est premier
   \return \a true si \a num est premier, \a false sinon
*/
int is_prime_number(int num) {
   for (int i = 2; i <= sqrt(num); i++) {
      if ((num % i) == 0) {
         return false;
      }
   }
   return true;
}

/*!
   \fn int lower_prime_number(int num)
   \brief Calcul du nombre premier directement inférieur à \a num
   \return Le nombre premier directement inférieur à \a num
*/
int lower_prime_number(int num) {
   if (num <= 3) {
      return 3;
   }
   int cnt = num - 1;
   while (!is_prime_number(cnt)) {
      cnt--;
   }
   return cnt;
}

/*!
   \fn int upper_prime_number(int num)
   \brief Calcul du nombre premier directement supérieur à \a num
   \return Le nombre premier directement supérieur à \a num
*/
int upper_prime_number(int num) {
   if (num < 3) {
      return 3;
   }
   int cnt = num + 1;
   while (!is_prime_number(cnt)) {
      cnt++;
   }
   return cnt;
}
