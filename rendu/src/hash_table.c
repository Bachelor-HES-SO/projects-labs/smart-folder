/*!
   \file hash_table.c
   \brief Contient les fonctions pour gérer une table de hash
   \author Abdennadher Raed & Antoniadis Orphée
   \version 0.1
   \date 27 janvier 2017
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../inc/prime_numbers.h"
#include "../inc/hash_table.h"

/*!
   \fn int get_hash(table_t* table, char* path)
   \brief Calcul d'un hachage pour \a path de fichier donnée
   \param table Table de hachage
   \param path Chemin d'un fichier
   \return Le hachage du path
*/
int get_hash(table_t* table, char* path) {
   int hash = 0;
	for (int i = 0; i < (int)strlen(path); i++) {
      hash = (hash + path[i]) % table->size;
   }
   return hash;
}

/*!
   \fn int get_double_hash(table_t* table, char* path)
   \brief Calcul d'un double hachage pour \a path de fichier donnée
   \param table Table de hachage
   \param path Chemin d'un fichier
   \return Le double hachage du path
*/
int get_double_hash(table_t* table, char* path) {
   int hash = get_hash(table,path);
   int double_hash_size = lower_prime_number(table->size);
   int double_hash = 1 + hash % double_hash_size;
   return double_hash;
}

/*!
   \fn element_t new_element(char* path, int hash)
   \brief Création d'un nouvel élément pour la table de hachage
   \param path Chemin d'un fichier
   \param hash Hachage du path
   \return La structure \a element_t
*/
element_t new_element(char* path, int hash) {
	element_t element;
	strcpy(element.path,path);
	element.hash = hash;
	element.state = 1;
	return element;
}

/*!
   \fn table_t* new_table(int size)
   \brief Création d'une table de hachage vide
   \param size Taille de la table de hachage
   \return Un pointeur sur une structure \a table_t
*/
table_t* new_table(int size) {
   table_t* table = malloc(sizeof(table_t));
   table->size = upper_prime_number(size);
   table->path_table = malloc(sizeof(element_t) * table->size);
	for (int i = 0; i < table->size; i++) {
		table->path_table[i].state = 0;
	}
   table->cnt = 0;
   return table;
}

/*!
   \fn void resize_table(table_t* table, int size)
   \brief Redimensionnement de la table de hachage
   \param table Ancienne table de hachage
   \param size Nouvelle taille de la table de hachage
*/
void resize_table(table_t* table, int size) {
   table_t* temp_table = new_table(size);
	table_copy(temp_table,table);
   free(table->path_table);
   table->size = temp_table->size;
   table->path_table = temp_table->path_table;
   free(temp_table);
}

/*!
   \fn void table_copy(table_t* dst, table_t* src)
   \brief Copie d'une table de hachage vers une autre
   \param dst Table de hachage copie
   \param src Table de hachage à copier
*/
void table_copy(table_t* dst, table_t* src) {
	for (int i = 0; i < src->size; i++) {
		if (src->path_table[i].state == 1) {
			insert_path(dst,src->path_table[i].path);
		}
	}
}

/*!
   \fn void clear_table(table_t* table)
   \brief Vidange de la table de hachage en mettant l'état de chaque élément à 0
   \param table Table de hachage à vider
*/
void clear_table(table_t* table) {
	for (int i = 0; i < table->size; i++) {
		table->path_table[i].state = 0;
	}
   table->cnt = 0;
}

/*!
   \fn int insert_path(table_t* table, char* path)
   \brief Insertion d'un path dans la table de hachage
   \param table Table de hachage
   \param path Path à inserer
   \return 1 si l'élément est inséré, 0 sinon
*/
int insert_path(table_t* table, char* path) {
   if (strlen(path) <= STR_SIZE) {
      if (table->cnt >= (table->size / 1.5)) {
         resize_table(table,(table->size * 1.5));
      }
      int hash = get_hash(table,path);
      int double_hash = get_double_hash(table,path);
      while (table->path_table[hash].state != 0) {
         if ((strcmp(path, table->path_table[hash].path) == 0)
				&& (table->path_table[hash].state == 1)) return 0;
         hash = (hash + double_hash) % table->size;
      }
      table->cnt++;
      table->path_table[hash] = new_element(path,hash);
      return 1;
   }
   return 0;
}

/*!
   \fn int search_path(table_t* table, char* path)
   \brief Recherche d'un path dans la table de hachage
   \param table Table de hachage
   \param path Path à chercher
   \return Le hachage si l'élément existe, -1 sinon
*/
int search_path(table_t* table, char* path) {
   int hash = get_hash(table,path);
   int double_hash = get_double_hash(table,path);
   while (strcmp(path, table->path_table[hash].path) != 0) {
      if (table->path_table[hash].state == 0) return -1;
      hash = (hash + double_hash) % table->size;
   }
   if (table->path_table[hash].state == 0) return -1;
   return hash;
}

/*!
   \fn void free_table(table_t* table)
   \brief Libération de la mémoire de la table de hachage
   \param table Table de hachage à libérer
*/
void free_table(table_t* table) {
   free(table->path_table);
   free(table);
}

/*!
   \fn void print_table(table_t* table)
   \brief Affichage de la table de hachage sur la sortie standard
   \param table Table de hachage à afficher
*/
void print_table(table_t* table) {
	for (int i = 0; i < table->size; i++) {
		if (table->path_table[i].state == 1) {
			printf("path : %s\n", table->path_table[i].path);
			printf("hash : %d\n\n", table->path_table[i].hash);
		}
	}
	printf("size : %d\n", table->size);
   printf("count : %d\n", table->cnt);
}

/*!
   \fn table_t* intersect_table(table_t* table1, table_t* table2)
   \brief Intersection de deux tables de hachage
   \param table1 Premier table de hachage
   \param table2 Deuxième table de hachage
   \return L'intersection entre les deux tables de hachage
*/
table_t* intersect_table(table_t* table1, table_t* table2) {
   int min_size = table1->size;
   if (table2->size < min_size) min_size = table2->size;
   table_t* final_table = new_table(min_size);
	for (int i = 0; i < table1->size; i++) {
		if (table1->path_table[i].state == 1) {
			if (search_path(table2,table1->path_table[i].path) != -1) {
	         insert_path(final_table,table1->path_table[i].path);
	      }
		}
	}
   return final_table;
}

/*!
   \fn table_t* union_table(table_t* table1, table_t* table2)
   \brief Union de deux tables de hachage
   \param table1 Premier table de hachage
   \param table2 Deuxième table de hachage
   \return L'union entre les deux tables de hachage
*/
table_t* union_table(table_t* table1, table_t* table2) {
   table_t* final_table = new_table(table1->size);
	table_copy(final_table,table1);
	table_copy(final_table,table2);
   return final_table;
}

/*!
   \fn table_t* substract_table(table_t* table, table_t* substracted_table) {
   \brief Soustraire une table de hachage d'une autre
   \param table Table de hachage à soustaire
   \param substracted_table Table de hachage depuis laquelle on soustrait
   \return La table de hachage soustraite
*/
table_t* substract_table(table_t* table, table_t* substracted_table) {
	int min_size = table->size;
   if (substracted_table->size < min_size) min_size = substracted_table->size;
   table_t* final_table = new_table(min_size);
	for (int i = 0; i < table->size; i++) {
		if (table->path_table[i].state == 1) {
			if (search_path(substracted_table,table->path_table[i].path) == -1) {
	         insert_path(final_table,table->path_table[i].path);
	      }
		}
	}
   return final_table;
}
