/*!
 	\file wrapper.c
 	\brief Contient les fonctions pour gérer la gestion des fichiers
	\author Abdennadher Raed & Antoniadis Orphée
 	\version 0.1
 	\date 27 janvier 2017
*/

#include "../inc/wrapper.h"

/*!
	\fn int my_mkdir(char* dir_path)
	\brief Création d'un nouveau répertoire
	\param dir_path Path du répertoire à créer
	\return 0 si tout est bien passé, 1 sinon
 */
int my_mkdir(char* dir_path) {
	if (mkdir(dir_path,0777) < 0) {
      	logger(LOG_DEBUG, stderr, "failed to create folder %s\n", dir_path);
      	return 1;
   	}
   	return 0;
}

/*!
	\fn int my_rmdir(char* dir_path)
	\brief Suppression d'un répertoire
	\param dir_path Path du répertoire à supprimer
	\return 0 si tout est bien passé, 1 sinon
 */
int my_rmdir(char* dir_path) {
	DIR* dir = my_opendir(dir_path);
	struct dirent* file = NULL;
	if (dir == NULL) return 1;
	while ((file = readdir(dir)) != NULL) {
		if (!(strcmp(file->d_name,".") == 0 || strcmp(file->d_name,"..") == 0)) {
			char file_path[MAXPATHLEN];
			strcpy(file_path, dir_path);
			strcat(file_path,"/");
			strcat(file_path,file->d_name);
			my_rm(file_path);
		}
	}
	my_closedir(dir);
   if (rmdir(dir_path) < 0) {
		logger(LOG_DEBUG, stderr, "failed to remove folder %s\n", dir_path);
      return 1;
   }
   return 0;
}

/*!
	\fn DIR* my_opendir(char* dir_path)
	\brief Ouverture d'un répertoire
	\param dir_path Path du répertoire à ouvrir
	\return Le répertoire ouvert, NULL sinon
 */
DIR* my_opendir(char* dir_path) {
	DIR* dir = NULL;
   if ((dir = opendir(dir_path)) == NULL) {
      logger(LOG_DEBUG, stderr, "failed to open folder %s\n", dir_path);
      return NULL;
   }
   return dir;
}

/*!
	\fn int my_closedir(DIR* dir)
	\brief Fermeture d'un répertoire
	\param dir Répertoire à fermer
	\return 0 si tout est bien passé, 1 sinon
 */
int my_closedir(DIR* dir) {
   if (closedir(dir) == -1) {
      logger(LOG_DEBUG, stderr, "failed to close folder\n");
      return 1;
   }
   return 0;
}

/*!
	\fn int my_rm(char* pathname)
	\brief Fermeture d'un fichier
	\param pathname Path du fichier à fermer
	\return 0 si tout est bien passé, 1 sinon
 */
int my_rm(char* pathname) {
	if (unlink(pathname) < 0) {
		logger(LOG_DEBUG, stderr, "failed to unlink file %s\n", pathname);
		return 1;
	}
	return 0;
 }

 /*!
	\fn int my_ln(char* oldpath, char* newpath)
	\brief Fermeture d'un fichier
	\param oldpath Path du fichier à lier
	\param newpath Path du lien
	\return 0 si tout est bien passé, 1 sinon
 */
int my_ln(char* oldpath, char* newpath) {
	int cnt = 1;
	if (symlink(oldpath,newpath) != 0) {
		while (errno == EEXIST) {
			char num[INTLEN], resolved_path[MAXPATHLEN];
			sprintf(num,"%d",cnt);
			strcpy(resolved_path,newpath);
			strcat(resolved_path,"-");
			strcat(resolved_path,num);
			if (symlink(oldpath,resolved_path) != 0) {
				if (errno != EEXIST) {
					logger(LOG_DEBUG, stderr, "failed to create symlink %s\n", resolved_path);
					return 1;
				}
			} else {
				errno = 0;
				return 0;
			}
			cnt++;
		}
		logger(LOG_DEBUG, stderr, "failed to create symlink %s\n", newpath);
		return 1;
	}
	return 0;
}

 /*!
	\fn void write_file(char* dir_path)
	\brief Enregistrement du PID du processus dans un fichier texte
	\param dir_path Path du fichier texte à enregistrer
	\param file_name Nom du fichier
 */
void write_file(char* dir_path, char* file_name) {
	char file_path[MAXPATHLEN], pid[INTLEN];
	strcpy(file_path,dir_path);
	strcat(file_path,file_name);
	sprintf(pid,"%d",getpid());
	FILE* stream = NULL;
	stream = fopen(file_path, "w");
	if (stream == NULL) {
		logger(LOG_DEBUG, stderr, "failed to open file %s\n", dir_path);
	} else {
		fputs(pid, stream);
		fclose(stream);
	}
}

 /*!
	\fn int read_file(char* dir_path) {
	\brief Lecture du PID du fichier texte
	\param dir_path Path du fichier texte à lire
 */
int read_file(char* dir_path, char* file_name) {
	char file_path[MAXPATHLEN], pid[INTLEN];
	strcpy(file_path,dir_path);
	strcat(file_path,file_name);
	FILE* stream = NULL;
	stream = fopen(file_path, "r");
	if (stream == NULL) {
		logger(LOG_DEBUG, stderr, "failed to open file %s\n", dir_path);
		return -1;
	} else {
		fgets(pid, INTLEN, stream);
		fclose(stream);
		return atoi(pid);
	}
}
