/*!
 	\file search_engine.c
 	\brief Contient les fonctions du moteur de recherche
	\author Abdennadher Raed & Antoniadis Orphée
 	\version 0.1
 	\date 27 janvier 2017
*/

#include "../inc/search_engine.h"

/*!
	\fn void* stop_search_engine(void* arg)
 	\brief Thread pour arrêter le programme

 	Ce thread va être lancé au démarrage du programme. Il va tourner tant
 	qu'il ne reçoit pas le signal SIGINT. S'il le reçoit, il va mettre
 	la valeur du pointeur en paramètre \a arg à \a 1.

 	\param arg Pointeur sur la variable qui permet au programme de
 		tourner en continue.
 	\return NULL
*/
void* stop_search_engine(void* arg) {
	sigset_t mask, maskold;
	sigfillset(&mask);
	pthread_sigmask(SIG_SETMASK, &mask, &maskold);
	int sig;
	do {
		sigwait(&mask, &sig);
	} while (sig != SIGINT);
	*((int*)arg) = 1;
	return NULL;
}

/*!
	\fn void search_engine(char* dir_path, char* search_path, expression_t* strct)
 	\brief Moteur de recherche

 	C'est la fonction qui représente le noyau du programme. Elle effectue les taches suivantes :
 	-# Création et lancement du processus de recherche (fork)
 	-# Création et lancement du thread de terminaison du programme
 	-# Création du fichier qui va contenir le PID du programme
 	-# Initialisation du table de hachage qui va contenir les hashs des paths dans le
 	dossier \a dir_path
 	-# Appel au crawler, filter, et puis linker pour finalement obtenir
 	les fichiers recherchés.

 	\param dir_path Le nom du répertoire du résultat de recherche
 	\param search_path Le nom du répertoire où la recherche va être effectuer
 	\param strct Structure qui contient les informations de critères de recherche
*/
void search_engine(char* dir_path, char* search_path, expression_t* strct) {
	int c_pid = -1;
	c_pid = fork();
	if (c_pid == 0) {
		sigset_t mask, maskold;
		sigfillset(&mask);
		pthread_sigmask(SIG_SETMASK, &mask, &maskold);
		pthread_t th_stop;
		int finished = 0;
		CHECK_ERR(pthread_create(&th_stop, NULL, stop_search_engine, &finished), "pthread_create failed!");
		write_file(dir_path, "/.pid.txt");
		table_t* newtable = new_table(LENGTH);
		table_t* oldtable = new_table(LENGTH);
		while (!finished) {
			clear_table(newtable);
			crawler(newtable, search_path);
			table_t* resolved_table = substract_table(newtable, oldtable);
			resolved_table = filter(resolved_table,strct);
			linker(oldtable, newtable, resolved_table, dir_path);
			clear_table(oldtable);
			table_copy(oldtable, newtable);
			free_table(resolved_table);
			sleep(1);
		}
		CHECK_ERR(pthread_join(th_stop, NULL), "pthread_join failed!");
		free_expression_struct(strct);
		free_table(oldtable);
		free_table(newtable);
	} else if (c_pid < 0) {
		perror("fork");
	}
}
