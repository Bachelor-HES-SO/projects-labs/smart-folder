/*!
   \file parser.c
   \brief Contient l'implémentation du logger
   \author Abdennadher Raed & Antoniadis Orphée
   \version 0.1
   \date 27 janvier 2017
*/

#include "../inc/logger.h"

/**
  \fn void logger(int log_level, FILE *destination, char *format, ...)
  \brief Afficher les messages de log sur le stream passé en argument
  \param log_level Le niveau de log
  \param destination Le flux sur lequel écrire (fichier, sortie/erreur
  	standard).
  \param format Le string à écrire.
  \param <unnamed> Une liste variable d'arguments (comme pour printf par
     exemple).
 */
void logger(int log_level, FILE *destination, char *format, ...) {
	va_list arguments;
	va_start(arguments, format);

	if (log_level <= LOG_LEVEL)
		vfprintf(destination, format, arguments);
	
	va_end(arguments);
}
