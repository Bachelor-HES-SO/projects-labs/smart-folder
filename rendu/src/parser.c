/*!
   \file parser.c
   \brief Contient les fonctions du parser des arguments
   \author Abdennadher Raed & Antoniadis Orphée
   \version 0.1
   \date 27 janvier 2017
*/

#include "../inc/parser.h"

const char* commandes[MAX_CMDS] = {"--name","--size","--date","--owner","--mode"};

/*!
   \fn criteria_t* init_name(char** expression, int index)
   \brief Initialisation d'une structure \a criteria_t avec le critère \a -\-name
   \param expression Tableau d'expression
   \param index Indice du critère (name ==> 0)
   \return Pointeur sur une structure \a criteria_t
*/
criteria_t* init_name(char** expression, int index) {
   criteria_t* criteria = malloc(sizeof(criteria_t));
   criteria->index = index;
   strcpy(criteria->str,expression[1]);
   return criteria;
}

/*!
   \fn criteria_t* init_size(char** expression, int index)
   \brief Initialisation d'une structure \a criteria_t avec le critère \a -\-size
   \param expression Tableau d'expression
   \param index Indice du critère (size ==> 1)
   \return Pointeur sur une structure \a criteria_t si tout est bien passé, NULL sinon.
*/
criteria_t* init_size(char** expression, int index) {
   if (strlen(expression[1]) > 2) {
      char first_char = expression[1][0];
      char last_char = expression[1][strlen(expression[1]) - 1];
      if ((first_char == '=' || first_char == '-' || first_char == '+') &&
            (toupper(last_char) == 'O' || toupper(last_char) == 'K' ||
               toupper(last_char) == 'M' || toupper(last_char) == 'G')) {
         criteria_t* criteria = malloc(sizeof(criteria_t));
         criteria->index = index;
         criteria->compare = expression[1][0];
         char temp[strlen(expression[1]) - 2];
         for (int i = 0; i < (int)(strlen(expression[1]) - 2); i++) {
            temp[i] = expression[1][i+1];
         }
         criteria->type = toupper(expression[1][strlen(expression[1]) - 1]);
         int val = atoi(temp);
         switch (criteria->type) {
            case 'K' :
               criteria->value = val * KILO;
               break;
            case 'M' :
               criteria->value = val * MEGA;
               break;
            case 'G' :
               criteria->value = val * GIGA;
               break;
            default :
               criteria->value = val;
         }
         return criteria;
      }
   }
   return NULL;
}

/*!
   \fn criteria_t* init_date(char** expression, int index)
   \brief Initialisation d'une structure \a criteria_t avec le critère \a -\-date
   \param expression Tableau d'expression
   \param index Indice du critère (date ==> 2)
   \return Pointeur sur une structure \a criteria_t si tout est bien passé, NULL sinon.
*/
criteria_t* init_date(char** expression, int index) {
   int date_size = 12;
   if ((int)strlen(expression[1]) == date_size) {
      char first_char = expression[1][0];
      char second_char = expression[1][1];
      if ((second_char == '=' || second_char == '-' || second_char == '+') &&
            (toupper(first_char) == 'C' || toupper(first_char) == 'M' ||
               toupper(first_char) == 'U')) {
         criteria_t* criteria = malloc(sizeof(criteria_t));
         criteria->index = index;
         criteria->type = toupper(expression[1][0]);
         criteria->compare = expression[1][1];
         strcpy(criteria->str,&expression[1][2]);
         return criteria;
      }
   }
   return NULL;
}

/*!
   \fn criteria_t* init_owner(char** expression, int index)
   \brief Initialisation d'une structure \a criteria_t avec le critère \a -\-owner
   \param expression Tableau d'expression
   \param index Indice du critère (owner ==> 3)
   \return Pointeur sur une structure \a criteria_t si tout est bien passé, NULL sinon.
*/
criteria_t* init_owner(char** expression, int index) {
   if ((int)strlen(expression[1]) > 2) {
      char first_char = expression[1][0];
      char second_char = expression[1][1];
      if ((second_char == '=' || second_char == '!') &&
            (toupper(first_char) == 'U' || toupper(first_char) == 'G')) {
         criteria_t* criteria = malloc(sizeof(criteria_t));
         criteria->index = index;
         criteria->type = toupper(expression[1][0]);
         criteria->compare = expression[1][1];
         strcpy(criteria->str,&expression[1][2]);
         return criteria;
      }
   }
   return NULL;
}

/*!
   \fn criteria_t* init_mode(char** expression, int index)
   \brief Initialisation d'une structure \a criteria_t avec le critère \a -\-mode
   \param expression Tableau d'expression
   \param index Indice du critère (mode ==> 4)
   \return Pointeur sur une structure \a criteria_t si tout est bien passé, NULL sinon.
*/
criteria_t* init_mode(char** expression, int index) {
   if ((int)strlen(expression[1]) == 4) {
      char first_char = expression[1][0];
      if ((toupper(first_char) == 'E' || toupper(first_char) == 'C') &&
          atoi(&expression[1][1]) != 0 ) {
         criteria_t* criteria = malloc(sizeof(criteria_t));
         criteria->index = index;
         criteria->type = expression[1][0];
         strcpy(criteria->str,&expression[1][1]);
         return criteria;
      }
   }
   return NULL;
}

/*!
   \fn criteria_t* criteria_gen(char** expression)
   \brief Génération d'une structure \a criteria_t selon le critère
   \param expression Tableau d'expression
   \return Pointeur sur une structure \a criteria_t
*/
criteria_t* criteria_gen(char** expression) {
   int index_cmd;
   for (index_cmd = 0; index_cmd < MAX_CMDS; index_cmd++) {
      if (strcmp(commandes[index_cmd],expression[0]) == 0) break;
   }
   criteria_t* criteria;
   switch (index_cmd) {
      case NAME : criteria = init_name(expression,index_cmd); break;
      case SIZE : criteria = init_size(expression,index_cmd); break;
      case DATE : criteria = init_date(expression,index_cmd); break;
      case OWNER : criteria = init_owner(expression,index_cmd); break;
      case MODE : criteria = init_mode(expression,index_cmd); break;
      default : logger(LOG_DEBUG, stderr, "Unknown command\n"); return NULL;
   }
   if (criteria == NULL) logger(LOG_DEBUG, stderr, "Wrong arguments\n");
   return criteria;
}

/*!
   \fn int is_condition(char* str)
   \brief Vérification si \a str est une condition ("not", "and" ou "or")
   \param str Chaine de caractères à vérifier
   \return \a true s'il s'agit d'une condition, \a false sinon.
*/
int is_condition(char* str) {
   if (strcmp(str,"not") == 0) return true;
   if (strcmp(str,"and") == 0) return true;
   if (strcmp(str,"or") == 0) return true;
   return false;
}

/*!
   \fn int is_condition(char* str)
   \brief Vérification si \a str est un critère (commençant par "-\-")
   \param str Chaine de caractères à vérifier
   \return \a true s'il s'agit d'un critère, \a false sinon.
*/
int is_criteria(char* str) {
   if (strlen(str) > 5) return (str[0] == '-' && str[1] == '-');
   else return false;
}

/*!
   \fn int get_nb_criterias(char** expression, int size) 
   \brief Calcul du nombre de critères dans \a expression
   \param expression Expression à parcourir
   \param size Taille du tableau d'expression
   \return \a Le nombre de critères
*/
int get_nb_criterias(char** expression, int size) {
   int cnt = 0;
   for (int i = 0; i < size; i ++) {
      if (is_criteria(expression[i])) cnt++;
   }
   return cnt;
}

/*!
   \fn int get_nb_conditions(char** expression, int size) 
   \brief Calcul du nombre de conditions dans \a expression
   \param expression Expression à parcourir
   \param size Taille du tableau d'expression
   \return \a Le nombre de conditions
*/
int get_nb_conditions(char** expression, int size) {
   int cnt = 0;
   for (int i = 0; i < size; i ++) {
      if (is_condition(expression[i])) cnt++;
   }
   return cnt;
}

/*!
   \fn expression_t* new_expression_struct(char** expression, int size) 
   \brief Allocation de la mémoire pour une structure \a expression_t
   \param expression Expression sous forme de tableau de chaînes de caractères
   \param size Taille du tableau d'expression
   \return Un pointeur su la structure \a expression_t alouée
*/
expression_t* new_expression_struct(char** expression, int size) {
   int nb_criterias = get_nb_criterias(expression,size);
   int nb_conditions = get_nb_conditions(expression,size);
   expression_t* strct = malloc(sizeof(expression_t));
   strct->nb_criterias = nb_criterias;
   strct->criterias = malloc(sizeof(criteria_t*) * strct->nb_criterias);
   strct->expression_size = nb_criterias + nb_conditions;
   strct->expression = malloc(sizeof(char*) * strct->expression_size);
   for (int i = 0; i < strct->expression_size; i++) {
      strct->expression[i] = malloc(sizeof(char) * STRLEN);
   }
   return strct;
}

/*!
   \fn expression_t* parse(char** expression, int size)
   \brief Analyse et construction de la structure \a expression_t

   Analyser les arguments passés au programme (sous forme de
   tableau de chaines de caractères \a char** \a expression) et construire
   la structure expression_t.

   \param expression Expression sous forme de tableau de chaînes de caractères
   \param size Taille du tableau d'expression
   \return Un pointeur sur la structure \a expression_t effective construite
*/
expression_t* parse(char** expression, int size) {
   expression_t* strct = new_expression_struct(expression,size);
   int expression_pos = 0;
   int criteria_pos = 0;
   for (int i = 0; i < size; i ++) {
      if (is_condition(expression[i])) {
         strcpy(strct->expression[expression_pos],expression[i]);
         expression_pos++;
      } else if (is_criteria(expression[i])) {
         sprintf(strct->expression[expression_pos],"%d",criteria_pos);
         criteria_t* temp_criteria = criteria_gen(&expression[i]);
         if (temp_criteria != NULL) {
            strct->criterias[criteria_pos] = temp_criteria;
            expression_pos++;
            criteria_pos++;
         }
      }
   }
   return strct;
}

/*!
   \fn void free_expression_struct(expression_t* strct)
   \brief Libération de la mémoire d'une structure \a expression_t
   \param strct Expression à libérée
*/
void free_expression_struct(expression_t* strct) {
	for (int i = 0; i < strct->nb_criterias; i++) {
      if (strct->criterias[i] != NULL) free(strct->criterias[i]);
	}
	free(strct->criterias);
	for (int i = 0; i < strct->expression_size; i++) {
		free(strct->expression[i]);
	}
	free(strct->expression);
	free(strct);
}
