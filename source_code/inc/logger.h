/*!
 	\file logger.h
 	\brief C'est le header du fichier logger.c
	\author Abdennadher Raed & Antoniadis Orphée
 	\version 0.1
 	\date 27 janvier 2017
*/

#ifndef _LOGGER_H_
/*!
  \def _PARSER_H_
  Pour éviter des inclusions multiples
*/
#define _LOGGER_H_

#include "wrapper.h"

#define LOG_ERROR 0
#define LOG_WARNING 1
#define LOG_INFO 2
#define LOG_DEBUG 3
/*!
  \def MAX_CMDS
  Le niveau actuel de log
*/
#define LOG_LEVEL LOG_DEBUG

void logger(int log_level, FILE *destination, char *format, ...) ;

#endif
