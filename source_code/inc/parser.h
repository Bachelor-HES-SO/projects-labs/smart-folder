/*!
 	\file parser.h
 	\brief C'est le header du fichier parser.c
	\author Abdennadher Raed & Antoniadis Orphée
 	\version 0.1
 	\date 27 janvier 2017
*/

#include "wrapper.h"

#ifndef _PARSER_H_
/*!
  \def _PARSER_H_
  Pour éviter des inclusions multiples
*/
#define _PARSER_H_

/*!
  \def MAX_CMDS
  Nombre de critères
*/
#define MAX_CMDS 		 5
/*!
  \def NAME
  Le critère --name
*/
#define NAME 			 0
/*!
  \def SIZE
  Le critère --size
*/
#define SIZE 			 1
/*!
  \def DATE
  Le critère --date
*/
#define DATE 			 2
/*!
  \def OWNER
  Le critère --owner
*/
#define OWNER 			 3
/*!
  \def MODE
  Le critère --mode
*/
#define MODE 			 4
/*!
  \def KILO
  1 kilo-octet
*/
#define KILO  		 1024
/*!
  \def MEGA
  1 méga-octet
*/
#define MEGA 	 1048576
/*!
  \def GIGA
  1 giga-octet
*/
#define GIGA 1073741824
/*!
  \def STRLEN
  Taille maximale des noms de fichiers
*/
#define STRLEN 80

/*!
	\brief
	Représente les informations des critères de recherche :

	|             | -\-name    | -\-size     | -\-date  | -\-owner | -\-mode     |
	| :---------- | :--------: | :---------: | :------: | :------: | :---------: |
	| index 	  | 0          | 1           | 2        | 3        | 4           |
	| str[STRLEN] | file_name  | null        | date     | owner    | mode_octal  | 
	| compare     | null       | +\|-\|=     | +\|-\|=  | +\|!     | null        | 
	| type        | null       | O\|K\|M\|G  | C\|M\|U  | U\|G     | E\|C        | 
	| value       | null       | size        | null     | null     | null        | 
*/
typedef struct {
	int index;
	char str[STRLEN]; 
	char compare; 
	char type; 
	long value;
} criteria_t;

/*!
	\brief
	Représente les critères de recherche et l'expression booléenne combinant
	les différents critères (sous la forme polonaise inverse)
*/
typedef struct {
	criteria_t** criterias; /**< Tableau de critères */
	int nb_criterias;		/**< Nombre de critères */
	char** expression;		/**< Tableau de chaines de caractères pour enregistrer l'expression booléenne */
	int expression_size;	/**< Taille du tableau d'expression */
} expression_t;

expression_t* parse(char** expression, int size);
void free_expression_struct(expression_t* strct);

#endif
