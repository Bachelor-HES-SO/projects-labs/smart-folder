/*!
 	\file crawler.c
 	\brief Contient les fonctions pour gérer le crawler
	\author Abdennadher Raed & Antoniadis Orphée
 	\version 0.1
 	\date 27 janvier 2017
*/

#include "../inc/search_engine.h"

/*!
	\fn char* get_filename(char* path)
	\brief Retourne le nom du fichier
	\param path Le chemin absolu vers le fichier
	\return Un string, le nom du fichier
 */
char* get_filename(char* path) {
   int cnt = strlen(path) - 1;
   while (path[cnt] != '/' && cnt != 0) cnt--;
   cnt++;
   return &path[cnt];
}

/*!
	\fn int is_dir(struct dirent* file)
	\brief Indique si un fichier est un repertoire
	\param file Un pointeur sur une entrée du repertoire
	\return 1 si l'entrée est un repertoire sinon 0
 */
int is_dir(struct dirent* file) {
	return (file->d_type == DT_DIR || file->d_type == DT_LNK);
}

/*!
	\fn void crawler(table_t* table, char* search_path)
	\brief Une fonction recursive qui va parcourir tous les fichiers sous \a search_path
	puis les insère dans une table de hash \a table
	\param table La table de hash qui contiendra tous les chemins vers les fichiers
	\param search_path Le sommet de l'arborescence à parcourir
 */
void crawler(table_t* table, char* search_path) {
	DIR* dir = my_opendir(search_path);
	struct dirent* file = NULL;
	if (dir == NULL) return;
	while ((file = readdir(dir)) != NULL) {
		if (!(strcmp(file->d_name,".") == 0 || strcmp(file->d_name,"..") == 0)) {
			char file_path[MAXPATHLEN], resolved_path[MAXPATHLEN];
			strcpy(file_path, search_path);
			strcat(file_path,"/");
			strcat(file_path,file->d_name);
			realpath(file_path,resolved_path);
			if (insert_path(table,resolved_path) && is_dir(file)) {
				crawler(table,file_path);
			}
		}
	}
	my_closedir(dir);
}
