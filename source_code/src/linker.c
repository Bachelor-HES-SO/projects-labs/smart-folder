/*!
 	\file linker.c
 	\brief Contient les fonctions pour gérer le linker
	\author Abdennadher Raed & Antoniadis Orphée
 	\version 0.1
 	\date 27 janvier 2017
*/

#include "../inc/search_engine.h"

/*!
   \fn int deleted_files(table_t* oldtable, table_t* newtable, int new_files_nb)
   \brief Indique si des éléments ont été supprimé du searchfolder
   \param oldtable Table de hash contenant les fichiers sous \a searchpath lors du dernier crawl
   \param newtable Table de hash contenant les fichiers sous \a searchpath lors du nouveau crawl
   \param new_files_nb Nombre de nouveaux éléments à ajouter au searchfolder
   \return 1 si des fichiers ont été supprimés sinon 0
*/
int deleted_files(table_t* oldtable, table_t* newtable, int new_files_nb) {
	return ((newtable->cnt - new_files_nb) < oldtable->cnt);
}

/*!
   \fn void refresh_links(table_t* oldtable, table_t* newtable, char* dir_path)
   \brief Efface les liens sur des fichiers qui ne doivent plus être dans le searchfolder
   \param oldtable Table de hash contenant les fichiers sous \a searchpath lors du dernier crawl
   \param newtable Table de hash contenant les fichiers sous \a searchpath lors du nouveau crawl
   \param dir_path Le nom du répertoire du résultat de recherche
*/
void refresh_links(table_t* oldtable, table_t* newtable, char* dir_path) {
	for (int i = 0; i < oldtable->size; i++) {
		if (oldtable->path_table[i].state == 1) {
			if (search_path(newtable, oldtable->path_table[i].path) == -1) {
				char* oldpath = oldtable->path_table[i].path;
				char* file_name = get_filename(oldpath);
				char newpath[strlen(dir_path) + strlen(file_name)];
				strcpy(newpath, dir_path);
				strcat(newpath, "/");
				strcat(newpath, file_name);
				my_rm(newpath);
			}
		}
	}
}

/*!
   \fn void linker(table_t* oldtable, table_t* newtable, table_t* resolved_table, char* dir_path)
   \brief Vérification si le nombre \a num est premier
   \param oldtable Table de hash contenant les fichiers sous \a searchpath lors du dernier crawl
   \param newtable Table de hash contenant les fichiers sous \a searchpath lors du nouveau crawl
   \param resolved_table Table de hash ne contenant que les nouveaux fichiers à ajouer au searchfolder
   \param dir_path Le nom du répertoire du résultat de recherche
*/
void linker(table_t* oldtable, table_t* newtable, table_t* resolved_table, char* dir_path) {
	if (deleted_files(oldtable, newtable, resolved_table->cnt)) {
		refresh_links(oldtable, newtable, dir_path);
	}
   for (int i = 0; i < resolved_table->size; i++) {
		if (resolved_table->path_table[i].state == 1) {
			char* oldpath = resolved_table->path_table[i].path;
			char* file_name = get_filename(oldpath);
			char newpath[strlen(dir_path) + strlen(file_name)];
			strcpy(newpath,dir_path);
			strcat(newpath,"/");
			strcat(newpath,file_name);
	      my_ln(oldpath, newpath);
		}
   }
}
