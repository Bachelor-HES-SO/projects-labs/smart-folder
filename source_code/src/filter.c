/*!
   \file filter.c
   \brief Contient les fonctions pour gérer le filtrage de la table de hachage
   selon les critères
   \author Abdennadher Raed & Antoniadis Orphée
   \version 0.1
   \date 27 janvier 2017
*/

#include "../inc/search_engine.h"

/*!
   \fn void filter_by_name(table_t* table, char* criteria_str, table_t* filtered_table)
   \brief Filtrer la tablea de hachage selon les noms des fichiers
   \param table Table de hachage qui contient tout les fichiers
   \param criteria_str Chaine de caractères cherchée
   \param filtered_table Table de hachage filtrée
 */
void filter_by_name(table_t* table, char* criteria_str, table_t* filtered_table) {
	for (int j = 0; j < table->size; j++) {
		if (table->path_table[j].state == 1) {
			char* temp_path = table->path_table[j].path;
			if (strstr(get_filename(temp_path), criteria_str) != NULL) {
				insert_path(filtered_table ,temp_path);
			}
		}
	}
}

/*!
   \fn void filter_by_size(table_t* table, criteria_t* criteria, table_t* filtered_table)
   \brief Filtrer la table de hachage selon les tailles des fichies
   \param table Table de hachage qui contient tout les fichiers
   \param criteria Structure qui contient les information recherché
   \param filtered_table Table de hachage filtrée
 */
void filter_by_size(table_t* table, criteria_t* criteria, table_t* filtered_table) {
	for (int j = 0; j < table->size; j++) {
		if (table->path_table[j].state == 1) {
			char* temp_path = table->path_table[j].path;
			struct stat statbuf;
			if (stat(temp_path, &statbuf) != -1) {
				long size = (long)statbuf.st_size;
				switch (criteria->compare) {
					case '+' :
						if (size > criteria->value) {
							insert_path(filtered_table ,temp_path);
						}
						break;
					case '-' :
						if (size < criteria->value) {
							insert_path(filtered_table ,temp_path);
						}
						break;
					case '=' :
						if (size == criteria->value) {
							insert_path(filtered_table ,temp_path);
						}
						break;
				}
			}
		}
	}
}

/*!
   \fn int compare_dates(struct tm* date1, struct tm* date2)
   \brief Comparer deux dates
   \param date1 Première date
   \param date2 Deuxième date
   \return 1 si \a date1 > \a date2, -1 si \a date1 < \a date2, 0 si égaux
 */
int compare_dates(struct tm* date1, struct tm* date2) {
	if (date1->tm_year > date2->tm_year) {
		return 1;
	}
	if (date1->tm_year < date2->tm_year) {
		return -1;
	}
	if (date1->tm_mon > date2->tm_mon) {
		return 1;
	}
	if (date1->tm_mon < date2->tm_mon) {
		return -1;
	}
	if (date1->tm_mday > date2->tm_mday) {
		return 1;
	}
	if (date1->tm_mday < date2->tm_mday) {
		return -1;
	}
	return 0;
}

/*!
   \fn void filter_by_date(table_t* table, criteria_t* criteria, table_t* filtered_table)
   \brief Filtrer la table de hache selon les dates
   \param table Table de hachage qui contient tout les fichiers
   \param criteria Structure qui contient les information recherché
   \param filtered_table Table de hachage filtrée
 */
void filter_by_date(table_t* table, criteria_t* criteria, table_t* filtered_table) {
	for (int j = 0; j < table->size; j++) {
		if (table->path_table[j].state == 1) {
			char* temp_path = table->path_table[j].path;
			struct stat statbuf;
			if (stat(temp_path, &statbuf) != -1) {
				struct tm* date;
				switch (toupper(criteria->type)) {
					case 'C' : //date du dernier changement de status
						date = localtime(&statbuf.st_ctime);
						break;
					case 'M' : //date de la dernière modification
						date = localtime(&statbuf.st_mtime);
						break;
					case 'U' : //date du dernier accès
						date = localtime(&statbuf.st_atime);
						break;
				}

				struct tm* criteria_date = malloc(sizeof(struct tm));
				strptime(criteria->str, "%d/%m/%Y", criteria_date);

				switch (criteria->compare) {
					case '+' :
						if (compare_dates(date, criteria_date) > 0) {
							insert_path(filtered_table ,temp_path);
						}
						break;
					case '-' :
						if (compare_dates(date, criteria_date) < 0) {
							insert_path(filtered_table ,temp_path);
						}
						break;
					default :
						if (compare_dates(date, criteria_date) == 0) {
							insert_path(filtered_table ,temp_path);
						}
				}
				free(criteria_date);
			}
		}
	}
}

/*!
   \fn void filter_by_owner(table_t* table, criteria_t* criteria, table_t* filtered_table)
   \brief Filtrer la table de hachage selon le propriétaire
   \param table Table de hachage qui contient tout les fichiers
   \param criteria Structure qui contient les information recherché
   \param filtered_table Table de hachage filtrée
 */
void filter_by_owner(table_t* table, criteria_t* criteria, table_t* filtered_table) {
	for (int j = 0; j < table->size; j++) {
		if (table->path_table[j].state == 1) {
			char* temp_path = table->path_table[j].path;
			struct stat statbuf;
			if (stat(temp_path, &statbuf) != -1) {
				char owner[80];
				switch (toupper(criteria->type)) {
					case 'U' :{
						struct passwd* pwd;
						if ((pwd = getpwuid(statbuf.st_uid)) != NULL) {
							strcpy(owner, pwd->pw_name);
						}
						break;
					}
					case 'G' :{
						struct group* grp;
					    if ((grp = getgrgid(statbuf.st_gid)) != NULL) {
					        strcpy(owner, grp->gr_name);
					    }
					   	break;
					}
				}
				switch (criteria->compare) {
					case '=' :
						if (strcmp(criteria->str, owner) == 0) {
							insert_path(filtered_table ,temp_path);
						}
						break;
					case '!' :
						if (strcmp(criteria->str, owner) != 0) {
							insert_path(filtered_table ,temp_path);
						}
						break;
				}
			}
		}
	}
}

/*!
   \fn char const* octal_permession(__mode_t mode)
   \brief Construction d'une chaine de caractère qui représente
  	les permession en octal
   \param mode Structure __mode_t à convertir
   \return Une chaine de caractère qui représente
  	les permession en octal
 */
char const* octal_permession(__mode_t mode) {
	static char perm_string[3];
	// user permissions
	int user_perm = 0;
	if ((mode & S_IRUSR) == S_IRUSR) user_perm += 4;
	if ((mode & S_IWUSR) == S_IWUSR) user_perm += 2;
	if ((mode & S_IXUSR) == S_IXUSR) user_perm += 1;
	// group permissions
	int group_perm = 0;
	if ((mode & S_IRGRP) == S_IRGRP) group_perm += 4;
	if ((mode & S_IWGRP) == S_IWGRP) group_perm += 2;
	if ((mode & S_IXGRP) == S_IXGRP) group_perm += 1;
	// other permissions
	int other_perm = 0;
	if ((mode & S_IROTH) == S_IROTH) other_perm += 4;
	if ((mode & S_IWOTH) == S_IWOTH) other_perm += 2;
	if ((mode & S_IXOTH) == S_IXOTH) other_perm += 1;

	sprintf(perm_string, "%d%d%d", user_perm, group_perm, other_perm);
	return perm_string;
}

/*!
   \fn void filter_by_mode_equals(table_t* table, criteria_t* criteria, table_t* filtered_table)
   \brief Filtrer la table de hachage selon les drois d'accès (les droits d'accés des fichiers sont
   égaux aux droits d'accés passés dans la structure \a criteria)
   \param table Table de hachage qui contient tout les fichiers
   \param criteria Structure qui contient les information recherché
   \param filtered_table Table de hachage filtré
 */
void filter_by_mode_equals(table_t* table, criteria_t* criteria, table_t* filtered_table) {
	for (int j = 0; j < table->size; j++) {
		if (table->path_table[j].state == 1) {
			char* temp_path = table->path_table[j].path;
			struct stat statbuf;
			if (stat(temp_path, &statbuf) != -1) {
				if (strcmp(criteria->str, octal_permession(statbuf.st_mode)) == 0) {
					insert_path(filtered_table, temp_path);
				}
			}
		}
	}
}

/*!
   \fn void filter_by_mode_contains(table_t* table, criteria_t* criteria, table_t* filtered_table)
   \brief Filtrer la table de hachage selon les drois d'accès (les droits d'accés des fichiers 
   contiennent les droits d'accés passés dans la structure \a criteria)
   \param table Table de hachage qui contient tout les fichiers
   \param criteria Structure qui contient les information recherché
   \param filtered_table Table de hachage filtré 
 */
void filter_by_mode_contains(table_t* table, criteria_t* criteria, table_t* filtered_table) {
	table_t* user_table;
	table_t* group_table;
	table_t* other_table;
	user_table = new_table(LENGTH);
	group_table = new_table(LENGTH);
	other_table = new_table(LENGTH);
	for (int j = 0; j < table->size; j++) {
		if (table->path_table[j].state == 1) {
			char* temp_path = table->path_table[j].path;
			struct stat statbuf;
			if (stat(temp_path, &statbuf) != -1) {
				if (criteria->str[0] == octal_permession(statbuf.st_mode)[0]) {
					insert_path(user_table, temp_path);
				}
				if (criteria->str[1] == octal_permession(statbuf.st_mode)[1]) {
					insert_path(group_table, temp_path);
				}
				if (criteria->str[2] == octal_permession(statbuf.st_mode)[2]) {
					insert_path(other_table, temp_path);
				}
			}
		}
	}
	if (criteria->str[0] == '9' && criteria->str[1] == '9') {
		table_copy(filtered_table, other_table);
	} else if (criteria->str[0] == '9' && criteria->str[2] == '9') {
		table_copy(filtered_table, group_table);
	} else if (criteria->str[1] == '9' && criteria->str[2] == '9') {
		table_copy(filtered_table, user_table);
	} else if (criteria->str[0] == '9') {
		filtered_table = intersect_table(group_table, other_table);
	} else if (criteria->str[1] == '9') {
		filtered_table = intersect_table(user_table, other_table);
	} else if (criteria->str[2] == '9') {
		filtered_table = intersect_table(user_table, group_table);
	}
}

/*!
   \fn table_t* filtered_table_gen(table_t* complete_table, criteria_t* criteria)
   \brief Construction de la table de hachage filtrée
   \param table Table de hachage qui contient tout les fichiers
   \param criteria Structure qui contient les information recherché
   \return La table de hachage filtrée
 */
table_t* filtered_table_gen(table_t* complete_table, criteria_t* criteria) {
	table_t* filtered_table = new_table(LENGTH);
	switch (criteria->index) {
		case NAME : filter_by_name(complete_table, criteria->str, filtered_table); break;
		case SIZE : filter_by_size(complete_table, criteria, filtered_table); break;
		case DATE : filter_by_date(complete_table, criteria, filtered_table); break;
		case OWNER : filter_by_owner(complete_table, criteria, filtered_table); break;
		case MODE : {
			switch (toupper(criteria->type)) {
				case 'E' :
					filter_by_mode_equals(complete_table, criteria, filtered_table);
					break;
				case 'C' : {
					filter_by_mode_contains(complete_table, criteria, filtered_table);
					break;
				}
			}
		}
	}
	return filtered_table;
}

/*!
   \fn table_t* reverse_polish(table_t* complete_table, table_t** filtered_tables, expression_t* strct) {
   \brief Filtrer la table de hachage selon plusieurs critères (en appliquant la logique
   de polonaise inverse)
   \param complete_table Table de hachage qui contient tout les fichiers
   \param filtered_tables Tableau de tables de hachage filtrée selon chaque critère
   \param strct Expression polonaise inverse à analyser
   \return La table de hachage finale filtrée
 */
table_t* reverse_polish(table_t* complete_table, table_t** filtered_tables, expression_t* strct) {
	node_t* list = new_list();
	table_t* final_filtered_table = new_table(LENGTH);
	int i = 0;
	while (i < strct->expression_size) {
		int criteria_pos = atoi(strct->expression[i]);
		if (criteria_pos != 0 || ((criteria_pos == 0) && (strcmp(strct->expression[i], "0") == 0))) {
			insert_tail(&list, filtered_tables[criteria_pos]);
		} else {
			if (strcmp(strct->expression[i], "not") == 0) {
				table_t* filtered_table = (table_t*) (remove_node(&list, 0).data);
				final_filtered_table = substract_table(complete_table, filtered_table);
			} else {
				table_t* table1 = (table_t*) (remove_node(&list, 0).data);
				table_t* table2 = (table_t*) (remove_node(&list, 0).data);
				if (strcmp(strct->expression[i],"and") == 0) {
					final_filtered_table = intersect_table(table1, table2);
				}
				if (strcmp(strct->expression[i],"or") == 0) {
					final_filtered_table = union_table(table1, table2);
				}
				insert_tail(&list, final_filtered_table);
			}
		}
		i++;
	}
	free_table(complete_table);
	return final_filtered_table;
}

/*!
   \fn table_t* filter(table_t* table, expression_t* strct) {
   \brief Filtrer la table de hachage
   \param table Table de hachage qui contient tout les fichiers
   \param strct Expression polonaise inverse à analyser
   \return La table de hachage finale filtrée
 */
table_t* filter(table_t* table, expression_t* strct) {
	if (table->size > table->cnt * 1.5) resize_table(table,table->cnt * 1.5);
	table_t* filtered_tables[strct->nb_criterias];
	for (int i = 0; i < strct->nb_criterias; i++) {
		if (strct->criterias[i] != NULL) {
			criteria_t* temp_criteria = strct->criterias[i];
			filtered_tables[i] = filtered_table_gen(table, temp_criteria);
		}
	}
	if (strct->nb_criterias == 1 && strct->expression_size == 1) {
		free_table(table);
		return filtered_tables[0];
	} else 
		return reverse_polish(table, filtered_tables, strct);
}
