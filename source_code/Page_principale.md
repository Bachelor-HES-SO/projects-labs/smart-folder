/* Documentation tag for Doxygen
 */

 \mainpage Documentation
 
  \section intro_sec Introduction
 
  Ce projet est conceptualisé dans le cadre du cours « Programmation Système ». Le but est de maitriser les appels systèmes sous Linux. \n\n

  Il s’agit d’un programme qui permet de chercher des fichiers et des dossiers selon des critères que l’utilisateur passe en argument, et de créer un répertoire dans lequel le résultat de la recherche va être enregistré sous forme de liens symboliques vers ces fichiers et dossiers.

 
  \section use_sec Utilisation

  #### Expressions : (on commence par --nomcomplet et on verra pour rajouter -caractère)

1. Nom du fichier :
  --name [string]

2. Taille du fichier :
  --size [+|-|=][int][O|K|M|G]
  Par défaut : =[int]O

3. Date de création/modification/utilisation du fichier :
  --date [C|M|U][+|-|=][Y/M/D]
  Par défaut : C=[Y/M/D]

4. Propriétaire/groupe de propriétaires du fichier :
  --owner [U|G][=|!][string]
  Par défaut : U=[string]

5. Droits d'accès du fichier :
  --mode [E|C][int][int][int]
  E pour equal, C pour contain. Pour gérer la vérification "contain", si la permission
  est le chiffre 9, ça veut dire qu'on va ignorer cette partie.
  Exemple :
  --mode C994 ==> revient à chercher les fichiers dont la permission contient 4 (r--) pour other
  sans tenir compte de la permission pour user et group. Autre exemple : --mode C009 ==> revient à chercher les fichiers dont la permission contient 0 pour user et 0 pour group sans tenir compte de la permission pour other

6. Expression booléenne combinant 1, 2, 3, 4, 5 : 
  Pour combiner deux expressions, il faut les écrire sous la forme polonaise inverse.\n
  Exemple : --date M+26/01/2017 --size =5O and --name toto or not
 
  \section demarche Démarche d'éxecution

  #### Démarche d'éxecution :

1. main -> vérification des arguments

2. on envoi la partie [expression] des arguments au parser

3. le parser parse l'ensemble [expression] en autant de sous ensemble qu'il y a de sous expressions.  
ex : [[name] and [date] or [size]], le parser va créer 3 structures qui contiendra les critères de chaque expression.

4. Le filtre va recevoir la table de hashage qu'aura généré le crawler. Le parser va ensuite envoyé le ou les structures au filtre qui va créer autant de listes de fichiers qu'il y a de structures à partir de la table.

5. le filtre va faire faire les opérations logique entre chaque liste.  
ex : ((name_list ∩ date_list) ∪ size_list)  

6. le filtre va envoyer la liste finale au linker qui va créer des liens symboliques dans le searchfolder.
