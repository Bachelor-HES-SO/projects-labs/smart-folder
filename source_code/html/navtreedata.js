var NAVTREE =
[
  [ "Smart Folder", "index.html", [
    [ "Documentation", "index.html", [
      [ "Introduction", "index.html#intro_sec", null ],
      [ "Utilisation", "index.html#use_sec", null ],
      [ "Démarche d'éxecution", "index.html#demarche", null ]
    ] ],
    [ "Page_principale", "md_Page_principale.html", null ],
    [ "Structures de données", "annotated.html", [
      [ "Structures de données", "annotated.html", "annotated_dup" ],
      [ "Index des structures de données", "classes.html", null ],
      [ "Champs de donnée", "functions.html", [
        [ "Tout", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Fichiers", null, [
      [ "Liste des fichiers", "files.html", "files" ],
      [ "Variables globale", "globals.html", [
        [ "Tout", "globals.html", null ],
        [ "Fonctions", "globals_func.html", null ],
        [ "Définitions de type", "globals_type.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html"
];

var SYNCONMSG = 'cliquez pour désactiver la synchronisation du panel';
var SYNCOFFMSG = 'cliquez pour activer la synchronisation du panel';